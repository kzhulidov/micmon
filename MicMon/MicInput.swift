//
//  MicInput.swift
//  MicMon
//
//  Created by Konstantin Zhulidov on 28/05/2021.
//

import Foundation
import AVFoundation
import Accelerate

enum MicInputState {
    case initializing
    case noInputDevice
    case noPermissionAsked
    case notPermitted
    case listening
    case listeningAndPlaying
}

class MicInput: ObservableObject {
    @Published var state: MicInputState = .initializing
    @Published var rmsDb: Float = 0.0
    private var engine: AVAudioEngine!
    
    init() {
        engine = AVAudioEngine()
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            startListening()
        case .notDetermined:
            state = .noPermissionAsked
        default:
            state = .notPermitted
        }
    }
    
    private func startListening() {
        let inputNode = engine.inputNode
        let bus = 0
        let format = inputNode.inputFormat(forBus: bus)
        if format.channelCount > 0 {
            inputNode.installTap(onBus: bus, bufferSize: 256, format: format) {
                (buffer: AVAudioPCMBuffer!, time: AVAudioTime!) -> Void in
                if let data = buffer.floatChannelData {
                    var rms: Float = 0.0
                    vDSP_rmsqv(data[0], buffer.stride, &rms, vDSP_Length(buffer.frameLength))
                    let value = 20 * log10(rms)
                    DispatchQueue.main.async {
                        self.rmsDb = value
                    }
                }
            }
            engine.prepare()
            start()
            state = .listening
        }
        else {
            state = .noInputDevice
        }
    }
    
    private func start() {
        try! engine.start()
    }
    
    private func stop() {
        engine.stop()
    }
    
    func requestAuthorization() {
        if state == .noPermissionAsked {
            AVCaptureDevice.requestAccess(for: .audio) { granted in
                DispatchQueue.main.async {
                    if granted {
                        self.startListening()
                    }
                    else {
                        self.state = .notPermitted
                    }
                }
            }
        }
    }
    
    func startPlaying() {
        if state == .listening {
            stop()
            engine.connect(engine.inputNode, to: engine.outputNode, format: nil)
            start()
            state = .listeningAndPlaying
        }
    }
    
    func stopPlaying() {
        if state == .listeningAndPlaying {
            stop()
            engine.disconnectNodeInput(engine.outputNode)
            start()
            state = .listening
        }
    }
}
