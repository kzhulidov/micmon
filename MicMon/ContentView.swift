//
//  ContentView.swift
//  MicMon
//
//  Created by Konstantin Zhulidov on 28/05/2021.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewModel: MicInput
    
    var body: some View {
        VStack {
            switch viewModel.state {
            case .initializing:
                Text("...")
            case .noInputDevice:
                Text("No input devices found")
            case .noPermissionAsked:
                Button("Request permission", action: viewModel.requestAuthorization)
            case .notPermitted:
                Text("Not permitted")
            case .listening:
                Button(action: viewModel.startPlaying){
                    Image("speaker_off")
                }
                    .buttonStyle(BorderlessButtonStyle())
                Text(String(format: "%.0f dB", viewModel.rmsDb))
                    .font(.title)
            case .listeningAndPlaying:
                Button(action: viewModel.stopPlaying) {
                    Image("speaker_on")
                }
                    .buttonStyle(BorderlessButtonStyle())
                Text(String(format: "%.0f dB", viewModel.rmsDb))
                    .font(.title)
            }
        }
        .frame(width: 200, height: 100)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: MicInput())
    }
}
